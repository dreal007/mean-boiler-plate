'use strict';

/**
 * @ngdoc overview
 * @name yomanApp
 * @description
 * # yomanApp
 *
 * Main module of the application.
 */
var app = angular.module('yomanApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ]);
  
  app.config(['$stateProvider', '$urlRouterProvider','$locationProvider', function ($stateProvider,  $urlRouterProvider, $locationProvider) {
	$locationProvider.html5Mode(true);  
    $urlRouterProvider.otherwise("main");
    $stateProvider
      .state('main', {
        url:'/main',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .state('about', {
        url:'/about',
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .state('user', {
        url:'/user',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user'
      });
	   
  
  }]);
